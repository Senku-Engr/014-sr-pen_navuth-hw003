
import java.util.ArrayList;
import java.util.Collection;
import java.util.*;
import java.util.Comparator;
import java.util.regex.Pattern;


public class Main {

    ArrayList<StaffMember> st;
    Scanner sc = new Scanner(System.in);
    int option, putID, putHours, deleteID, editID;
    String putName, putAddress;
    double putRate, putSalary, putBonus;

    Main() {
        st = new ArrayList<StaffMember>();

        st.add(new Volunteer(1, "Mitsuha", "Japan"));
        st.add(new HourlyEmployee(2, "Hodoka", "Japan", 4, 20.0));
        st.add(new SalariedEmployee(3, "Hina", "Japan", 5000, 500));
        st.add(new SalariedEmployee(4, "Taki", "Japan", 5000, 500));
    }

    public boolean regexNum(String s) {
        return Pattern.matches("\\d", s);
    }

    public boolean regexNums(String s) {
        return Pattern.matches("\\d+", s);
    }

    public boolean regexName(String s) {
        return Pattern.matches("\\D+", s);
    }

    public boolean regexFloat(String s) {
        return Pattern.matches("[0-9]+(\\.[0-9]+)?([0-9]+)?", s);
    }

    public int getHoursWorked() {
        System.out.print(" ==> Enter Staff Member's Worked Hours : ");
        String s = sc.nextLine();
        if (regexNums(s) == true) {
            putHours = Integer.parseInt(s);
        } else {
            getHoursWorked();
        }
        return putHours;
    }

    public double getRate() {
        System.out.print(" ==> Enter Staff Member's Rate : ");
        String s = sc.nextLine();
        if (regexFloat(s) == true) {
            putRate = Double.parseDouble(s);
        } else {
            getRate();
        }
        return putRate;
    }

    public double getSalary() {
        System.out.print(" ==> Enter Staff Member's Salary : ");
        String s = sc.nextLine();
        if (regexFloat(s) == true) {
            putSalary = Double.parseDouble(s);
        } else {
            getSalary();
        }
        return putSalary;
    }

    public double getBonus() {
        System.out.print(" ==> Enter Staff Member's Bonus : ");
        String s = sc.nextLine();
        if (regexFloat(s) == true) {
            putBonus = Double.parseDouble(s);
        } else {
            getBonus();
        }
        return putBonus;
    }

    public String getName() {
        System.out.print(" ==> Enter Staff Member's Name : ");
        String s = sc.nextLine();
        if (regexName(s) == true) {
            putName = s;
        } else {
            getName();
        }
        return putName;
    }

    public String getAddress() {
        System.out.print(" ==> Enter Staff Member's Address : ");
        String s = sc.nextLine();
        if (regexName(s) == true) {
            putAddress = s;
        } else {
            getAddress();
        }
        return putAddress;
    }

    public int getID() {
        int count = 0;
        System.out.print(" ==> Enter Staff Member's ID : ");
        String s = sc.nextLine();
        if (regexNums(s) == true) {
            int opt = Integer.parseInt(s);
            for (StaffMember a : st) {
                if (a.id == opt) {
                    count++;
                }
            }
            if (count == 0) {
                putID = opt;
            } else {
                System.err.println("    This ID already Exit please put another ID");
                getID();
            }
        } else {
            getID();
        }
        return putID;
    }

    public int getDeleteID() {
        System.out.print(" ==> Enter Employee ID to Remove : ");
        String s = sc.nextLine();
        if (regexNums(s) == true) {
            deleteID = Integer.parseInt(s);
        } else {
            getDeleteID();
        }
        return deleteID;
    }

    public int getEditID() {
        System.out.print(" ==> Enter Employee ID to Edit : ");
        String s = sc.nextLine();
        if (regexNums(s) == true) {
            editID = Integer.parseInt(s);
        } else {
            getEditID();
        }
        return editID;
    }


    public int getOption() {
        System.out.print("Choose option[1-4] : ");
        String s = sc.nextLine();
        if (regexNum(s) == true) {
            int opt = Integer.parseInt(s);
            if (opt >= 0 && opt <= 4) {
                option = opt;
            } else {
                getOption();
            }
        } else {
            getOption();
        }
        return option;
    }

    public void addVolunteer() {
        System.out.println("=========== Insert Volunteer Info ===========");
        int id = getID();
        String name = getName();
        String address = getAddress();
        st.add(new Volunteer(id, name, address));
        System.out.println();
        System.out.println("    Added Successfully");
        System.out.println();
    }

    public void addHourlyEmp() {
        System.out.println("============ Insert Hourly Employee Info =========== ");
        int id = getID();
        String name = getName();
        String address = getAddress();
        int hours = getHoursWorked();
        double rate = getRate();
        st.add(new HourlyEmployee(id, name, address, hours, rate));
        System.out.println();
        System.out.println("    Added Successfully");
        System.out.println();
    }

    public void addSalariedEmp() {
        System.out.println("=========== Insert Salaried Employee Info ==========");
        int id = getID();
        String name = getName();
        String address = getAddress();
        double salary = getSalary();
        double bonus = getBonus();
        st.add(new SalariedEmployee(id, name, address, salary, bonus));
        System.out.println();
        System.out.println("    Added Successfully");
        System.out.println();
    }


    public void addEmployee() {
        System.out.println("=====================");
        System.out.println("1). Volunteer    2). Hourly Emp    3). Salaried Emp  4). Back");
        int opt = getOption();
        switch (opt) {
            case 1:
                addVolunteer();
                menu();
                break;
            case 2:
                addHourlyEmp();
                menu();
                break;
            case 3:
                addSalariedEmp();
                menu();
                break;
            case 4:
                menu();
                break;
        }

    }

    public void menu() {
        printInformation();
        System.out.println("==================================");
        System.out.println("1). Add Employee    2). Edit    3). Remove  4). Exit");
        int opt = getOption();
        switch (opt) {
            case 1:
                System.out.println("-----------------------------");
                addEmployee();
                break;
            case 2:
                editEmp();
                menu();
                break;
            case 3:
                removeEmp();
                menu();
                break;
            case 4:
                System.out.println();
                System.out.println("    Goodbye ");
                System.exit(0);
                break;
        }

    }

    public void printInformation() {
        Collections.sort(st, Main.StuNameComparator);
        for (StaffMember a : st) {
            System.out.println();
            a.ToString();
            System.out.println("==================================");
        }
    }

    public static Comparator<StaffMember> StuNameComparator = new Comparator<StaffMember>() {

        public int compare(StaffMember s1, StaffMember s2) {
            String StudentName1 = s1.name.toUpperCase();
            String StudentName2 = s2.name.toUpperCase();

            //ascending order
            return StudentName1.compareTo(StudentName2);

            //descending order
            //return StudentName2.compareTo(StudentName1);
        }
    };

    public void editEmp() {
        int id = getEditID();
        int count = 0;
        for (StaffMember a : st) {
            if (a.id == id) {
                count++;
            }
        }

        if (count != 0) {
            Iterator itr = st.iterator();
            while (itr.hasNext()) {
                StaffMember ss = (StaffMember) itr.next();
                if (ss.id == id) {
                    System.out.println();
                    ss.ToString();
                    System.out.println();
                    itr.remove();
                    if (ss instanceof SalariedEmployee) {
                        String name = getName();
                        String address = getAddress();
                        double salary = getSalary();
                        double bonus = getBonus();
                        st.add(new SalariedEmployee(id, name, address, salary, bonus));


                    } else if (ss instanceof HourlyEmployee) {
                        String name = getName();
                        String address = getAddress();
                        int hour = getHoursWorked();
                        double rate = getRate();
                        st.add(new HourlyEmployee(id, name, address, hour, rate));


                    } else if (ss instanceof Volunteer) {
                        String name = getName();
                        String address = getAddress();
                        st.add(new Volunteer(id, name, address));

                    }
                    System.out.println();
                    System.out.println("    Edit Successfully");
                    System.out.println();
                    break;
                }
            }
        } else {
            System.out.println();
            System.err.println("    This ID does not Exist");
            System.err.println("    Fail to Edit Employee");
            System.out.println();
        }

    }


    public void removeEmp() {
        System.out.println("============= Remove Staff ==============");
        int count = 0;
        int id = getDeleteID();

        for (StaffMember a : st) {
            if (a.id == id) {
                count++;
            }
        }

        if (count != 0) {
            Iterator itr = st.iterator();
            while (itr.hasNext()) {
                StaffMember ss = (StaffMember) itr.next();
                if (ss.id == id) {
                    itr.remove();
                    ss.ToString();
                    System.out.println();
                    System.out.println("    Removed Successfully ");
                    System.out.println();
                }
            }
        } else {
            System.out.println();
            System.err.println("    This ID does not Exist");
            System.err.println("    Fail to remove Employee");
            System.out.println();
        }

    }

    public static void main(String[] args) {

        Main m = new Main();
        m.menu();

    }
}
