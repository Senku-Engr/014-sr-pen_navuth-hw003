public abstract class StaffMember {
    protected int id;
    protected String name,address;

    StaffMember(int id, String name, String address){
        this.id = id;
        this.name = name;
        this.address = address;
    }
    public void ToString(){
        System.out.println("ID : " + this.id);
        System.out.println("Name : " + this.name);
        System.out.println("Address : " + this.address);
    }
    abstract double pay();
}