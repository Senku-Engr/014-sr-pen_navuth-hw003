public class HourlyEmployee extends StaffMember {
   private int hoursWorked;
   private double rate;
   public void setHoursWorked(int hoursWorked){
       this.hoursWorked = hoursWorked;
   }
   public void setRate(double rate){
       this.rate = rate;
   }
   public int getHoursWorked(){
       return hoursWorked;
   }
   public double getRate(){
       return rate;
   }
   HourlyEmployee(int id, String name, String address, int hoursWorked, double rate){
       super(id, name, address);
       setHoursWorked(hoursWorked);
       setRate(rate);
   }
   public void ToString(){
       super.ToString();
       System.out.println("Hours worked : " + hoursWorked);
       System.out.println("Rate : " + rate);
       System.out.println("Payment : "+ pay());
   }
   public double pay(){
    return hoursWorked*rate;
   }
}
