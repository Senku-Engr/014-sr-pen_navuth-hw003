public class SalariedEmployee extends StaffMember {
    private double salary, bonus;

    public double getSalary(){
        return salary;
    }
    public double getBonus(){
        return bonus;
    }
    public void setSalary(double salary) {
        this.salary = salary;
    }

    public void setBonus(double bonus) {
        this.bonus = bonus;
    }

    SalariedEmployee(int id, String name, String address, double salary, double bonus) {
        super(id, name, address);
        setBonus(bonus);
        setSalary(salary);
    }
    public void ToString(){
        super.ToString();
        System.out.println("Salary : " + getSalary());
        System.out.println("Bonus : " + getBonus());
        System.out.println("Payment : "+ pay());

    }
    public double pay() {
        return salary+bonus;
    }
}
